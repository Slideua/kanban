var kanbanBoard = angular.module('kanbanBoard', ['dndLists', 'contenteditable', 'chart.js']);

function Sheet(title, content, timeSpend) {
    this.title = title;
    this.content = content;
    this.timeSpend = timeSpend;
    this.sheetEditable = false;
}

function Column(slug, title, estimate) {
    this.title = title;
    this.slug = slug;
    this.sheets = [];
    this.editable = false;
    this.visibleSheets = true;
    this.estimate = estimate;
    this.addSheet = function (title, content, timeSpend) {
        this.sheets.push(new Sheet(title, content, timeSpend));
    };

    this.toggleVisibleSheets = function () {
        this.visibleSheets = !this.visibleSheets;
    };

    this.removeAllSheets = function () {
        var confirmDelete = window.confirm('Are you sure you want to delete a sheets?');
        if(confirmDelete){
            this.sheets = [];
        }
    }
}

var getStubColumns = function () {
    var colWithSheets = new Column('inProgress', 'in progress', 88);
    colWithSheets.addSheet('first list title', 'first list content', {inProgress: 120, readyToTest: 14, tested: 88});
    colWithSheets.addSheet('second list title', 'second list content', {inProgress: 132, readyToTest: 14, tested: 88});
    colWithSheets.editable = true;

    return [
        colWithSheets
        , new Column('readyToTest', 'ready to test', 14)
        , new Column('tested', 'tested', 67)
    ];
};

var getTrash = function () {
    var trash = new Column('trash', 'Trash');
    trash.visibleSheets = false;
    return trash;
};

