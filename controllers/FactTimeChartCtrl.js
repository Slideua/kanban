kanbanBoard.controller("FactTimeChartCtrl", function ($scope, columnsService) {
    var columns = columnsService;
    $scope.labels = columns.getColumnsTitles();
    $scope.data = columns.getAllTime();
});