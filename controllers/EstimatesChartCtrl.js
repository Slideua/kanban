kanbanBoard.controller("EstimatesChartCtrl", function ($scope, columnsService) {
    var columns = columnsService;
    $scope.labels = columns.getColumnsTitles();
    $scope.data = columns.getEstimates();
});