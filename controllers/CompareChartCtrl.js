kanbanBoard.controller("CompareChartCtrl", function ($scope, columnsService) {
    var columns = columnsService;
    $scope.columns = columns;
    $scope.labels = columns.getColumnsTitles();
    $scope.data = [
        columns.getAllTime(),
        columns.getEstimates()
    ];
});
