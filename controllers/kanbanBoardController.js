kanbanBoard.controller('kanbanBoardController', function ($scope, columnsService) {
    $scope.columns = getStubColumns();
    $scope.trash = getTrash();
    columnsService.addColumnsData($scope.columns);
});