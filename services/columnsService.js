kanbanBoard.service('columnsService', function() {
    var columns = [];

    var addColumnsData = function(colData) {
        columns = colData;
    };

    var getColumnsData = function() {
        return columns;
    };

    var getColumnsTitles = function() {
        var columnsTitles = [];
        for(var i = 0; i < columns.length; i++) {
            columnsTitles.push(columns[i].title);
        }
        return columnsTitles;
    };

    var getEstimates = function() {
        var estimates = [];
        for(var i = 0; i < columns.length; i++) {
            estimates.push(columns[i].estimate);
        }
        return estimates;
    };

    var getAllTime = function() {
        var sheets = getSheets();
        return [
          calculateSheetsTime('inProgress', sheets)
          , calculateSheetsTime('readyToTest', sheets)
          , calculateSheetsTime('tested', sheets)
        ]
    };

    var calculateSheetsTime = function(phase, sheets) {
        return sheets.map(function(sheet) {
            return sheet.timeSpend[phase];
        }).filter(function(phaseTimeSpend) {
            return phaseTimeSpend != undefined && phaseTimeSpend != null;
        }).reduce(function(sum, current) {
            return sum + parseInt(current);
        }, 0);
    };

    var union = function(fa, sa) {
        var result = [];
        fa.forEach(function(e) {result.push(e);});
        sa.forEach(function(e) {result.push(e);});
        return result;
    };

    var getSheets = function() {
        return columns.map(function(column) {
           return column.sheets;
        }).reduce(union, []);
    };

    return {
        addColumnsData: addColumnsData,
        getColumnsData: getColumnsData,
        getEstimates: getEstimates,
        getColumnsTitles: getColumnsTitles,
        getAllTime: getAllTime
    };
});